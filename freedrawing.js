var $ = function(id){return document.getElementById(id)};

var canvas = this.__canvas = new fabric.Canvas('c', {
  isDrawingMode: true
  });
  
 /////CAPTURANDO A RESOLUCAO DA AREA DO NAVEGADOR/////
function viewport()
{
	var e = window
	, a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
}

canvas.setHeight (viewport().height-25); 
canvas.setWidth (viewport().width-180);

window.onresize = function(){
	canvas.setHeight (viewport().height-25); 
	canvas.setWidth (viewport().width-180);
}
	
 //////////////////////////////////////////////////////////////////////

fabric.Object.prototype.transparentCorners = false;
  
var cor= "#3399FF", linha=10;
canvas.freeDrawingBrush.color=cor;
canvas.freeDrawingBrush.width =linha;
  
var drawingModeEl = $('drawing-mode'),
	saveAndClearEl =$('saveAndClear'),
	colorOptionsEl = $('colors'),
	formsOptionsEl = $('autoformas'),
	drawingColorEl = $('custom-color'),
	fillColorEl = $('bucket-tool'),
	clearEl = $('clear-canvas'),
	saveEl = $('save'),
	backgroundColorEl = $('background-color'),
	delEl = $('delete'),
	duplicateEl = $('duplicate'),
	forwardEl = $('send-forward'),
	backwardsEl = $('send-backwards'),
	
	drawSquareEl = $('draw-square'),
	drawCircleEl = $('draw-circle'),
	drawTriangleEl= $('draw-triangle'),
	drawStarEl= $('draw-star'),
	drawDiamondEl= $('draw-diamond'),
	drawRightTriangleEl= $('draw-rTriangle');
	
  
//////////MODO EDICAO/DESENHO//////////
drawingModeEl.onclick = function() {
	canvas.isDrawingMode = !canvas.isDrawingMode;
	if (canvas.isDrawingMode) {
	  drawingModeEl.innerHTML = 'Editar desenho';
	  saveAndClearEl.style.display = '';
	  formsOptionsEl.style.display = 'none';
	}
	else {
	  drawingModeEl.innerHTML = 'Desenhar';
	  saveAndClearEl.style.display = 'none';
	  formsOptionsEl.style.display = '';
	}
};	  
	  
//////////LIMPA TELA//////////
clearEl.onclick = function() {
	var m = confirm("Apagar desenho e limpar a tela?");
		if (m) {
		canvas.clear()
		canvas.backgroundColor='';
		canvas.renderAll();
		}
};
  
//////////SALVAR//////////
saveEl.onclick = function() {
	canvas.deactivateAll().renderAll();
	var data = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
	this.href = data;
};

//////////COR DE FUNDO DA TELA//////////
backgroundColorEl.onclick = function() {
	canvas.backgroundColor=cor;
	canvas.renderAll();
};

//////////DELETA OBJETOS//////////
delEl.onclick = function() {
	if (canvas.getActiveGroup())
	{
		canvas.getActiveGroup().forEachObject(function(o){ canvas.remove(o) });
		canvas.discardActiveGroup().renderAll();
	} 
	else {
		canvas.remove(canvas.getActiveObject());
		event.preventDefault();
	}
};

//////////DUPLICA OBJETOS//////////
duplicateEl.onclick = function() {
	if (canvas.getActiveObject() || canvas.getActiveGroup()){	
		copy(); paste();
		}
};

//////////PREENCHE COM COR//////////
fillColorEl.onclick = function() {
	if (canvas.getActiveObject()){
		var object = canvas.getActiveObject();
		object.set({fill:fillColorEl.color=cor});
		canvas.renderAll();
	}	
	else	if (canvas.getActiveGroup()){
		canvas.getActiveGroup().forEachObject(function(o){ o.set({fill:fillColorEl.color=cor})});
		canvas.renderAll();
	}
};

//////////TRAZER OBJETOS PARA FRENTE//////////
forwardEl.onclick = function() {
	if (canvas.getActiveObject()){
		var object = canvas.getActiveObject();
		canvas.bringToFront(object); 
		canvas.renderAll();
	}	
};

//////////ENVIA OBJETOS PARA TRAZ//////////
backwardsEl.onclick = function() {
	if (canvas.getActiveObject()){
		var object = canvas.getActiveObject();
		canvas.sendToBack(object); 
		canvas.renderAll();
	}	
};
					
//////////COR PERSONALIZADA//////////				
drawingColorEl.onchange = function() {
	canvas.freeDrawingBrush.color = '#'+this.color;
	cor=canvas.freeDrawingBrush.color;
	if (canvas.getActiveObject()){
		var object = canvas.getActiveObject();
		object.set({stroke:cor});
		canvas.renderAll();
	}	
	else	if (canvas.getActiveGroup()){
		canvas.getActiveGroup().forEachObject(function(o){ o.set({stroke:cor})});
		canvas.renderAll();
	}
};
drawingColorEl.onclick = function() {
	canvas.freeDrawingBrush.color = '#'+this.color;
	cor=canvas.freeDrawingBrush.color;
	if (canvas.getActiveObject()){
		var object = canvas.getActiveObject();
		object.set({stroke:cor});
		canvas.renderAll();
	}	
	else	if (canvas.getActiveGroup()){
		canvas.getActiveGroup().forEachObject(function(o){ o.set({stroke:cor})});
		canvas.renderAll();
	}
};

////////////DESENHANDO FIGURAS GEOMETRICAS////////////
drawSquareEl.onclick = function() {	
	var rect = new fabric.Rect({width: 150, height: 150, strokeWidth:(linha),stroke: (cor), fill:(''), left: 50, top: 50});
	canvas.add(rect);
}
drawCircleEl.onclick = function(){
	var circle = new fabric.Circle({ radius: 75, strokeWidth:(linha),stroke: (cor), fill:(''), left: 50, top: 50});
	canvas.add(circle);
}
drawTriangleEl.onclick = function(){
	var triangle = new fabric.Triangle({width: 150, height: 150,  strokeWidth:(linha),stroke: (cor), fill:(''), left: 50, top: 50});
	canvas.add(triangle);
}
drawStarEl.onclick = function(){
	var star = new fabric.Star({numPoints:5, innerRadius:50, outerRadius:100, left:35, top:35,strokeWidth:(linha),stroke: (cor),fill:('')});
	canvas.add(star);
}
drawDiamondEl.onclick = function(){
	var diamond = new fabric.Polygon(
	[{x: 75, y: 0}, {x: 120, y: 75}, {x: 75, y: 150}, {x: 30, y: 75} ],
	{ strokeWidth:(linha),stroke: (cor), fill:(''), left: 80, top: 50});
	canvas.add(diamond);
}
drawRightTriangleEl.onclick = function(){
	var rTriangle = new fabric.Polygon(
	[{x: 0, y: 150}, {x: 150, y: 150}, {x: 150, y: 0} ],
	{ strokeWidth:(linha),stroke: (cor), fill:(''), left: 50, top: 50});
	canvas.add(rTriangle);
}
	 
////////////FUNCOES CTRL+C, CTRL+V E DEL NO TECLADO////////////
createListenersKeyboard();

function createListenersKeyboard() {
	document.onkeydown = onKeyDownHandler;
	//document.onkeyup = onKeyUpHandler;
}
function onKeyDownHandler(event) {
//event.preventDefault();

	var key;
	if(window.event){
		key = window.event.keyCode;
	}
	else{
		key = event.keyCode;
	}
	
	switch(key){
		//////////////
		// Shortcuts
		//////////////
		// Copy (Ctrl+C)
		case 67: // Ctrl+C
			if(ableToShortcut()){
				if(event.ctrlKey){
					event.preventDefault();
					copy();
				}
			}
			break;
		// Paste (Ctrl+V)
		case 86: // Ctrl+V
			if(ableToShortcut()){
				if(event.ctrlKey){
					event.preventDefault();
					paste();
				}
			}
			break;   
		// Delete 
		case 46:  //Del
			
			if (canvas.getActiveGroup()){
				canvas.getActiveGroup().forEachObject(function(o){ canvas.remove(o) });
				canvas.discardActiveGroup().renderAll();
			} else {
				canvas.remove(canvas.getActiveObject());
				event.preventDefault();
				}
		break;  
			
		default:
			// TODO
			break;
	}
}

function ableToShortcut(){
	/* TODO check all cases for this
	if($("textarea").is(":focus")){
		return false;
	}
	if($(":text").is(":focus")){
		return false;
	} */
	return true;
}
	
function copy(){
	if(canvas.getActiveGroup()){
		for(var i in canvas.getActiveGroup().objects){
			var object = fabric.util.object.clone(canvas.getActiveGroup().objects[i]);
			object.set("top", object.top+5);
			object.set("left", object.left+5);
			copiedObjects[i] = object;
		}                    
	}
	else if(canvas.getActiveObject()){
		var object = fabric.util.object.clone(canvas.getActiveObject());
		object.set("top", object.top+5);
		object.set("left", object.left+5);
		copiedObject = object;
		copiedObjects = new Array();
	}
}

function paste(){
	if(copiedObjects.length > 0){
		for(var i in copiedObjects){
			canvas.add(copiedObjects[i]);
		}                    
	}
	else if(copiedObject){
		canvas.add(copiedObject);
	}
	canvas.renderAll();    
}

//////////SELECIONA COR DA PALHETA//////////	
function colorDrawing(colorvalue){
	canvas.freeDrawingBrush.color= colorvalue ;
	cor = colorvalue;
	if (canvas.getActiveObject()){
			var object = canvas.getActiveObject();
			object.set({stroke:cor});
			canvas.renderAll();
	}	
	else	if (canvas.getActiveGroup()){
			canvas.getActiveGroup().forEachObject(function(o){ o.set({stroke:cor})});
			canvas.renderAll();
	}
};

//////////SELECIONA LINHA DA BARRA DE FERRAMENTAS//////////
function changeLineWidth(linevalue) {
	linha = linevalue;
	canvas.freeDrawingBrush.width =linha;
	if (canvas.getActiveObject()){
		var object = canvas.getActiveObject();
		object.set({strokeWidth:linha});
		canvas.renderAll();
	}	
	else	if (canvas.getActiveGroup()){
		canvas.getActiveGroup().forEachObject(function(o){ o.set({strokeWidth:linha})});
		canvas.renderAll();
	}
};